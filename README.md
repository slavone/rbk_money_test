# RbkTest

Основной код находится в lib/rbk_test/guild.ex и lib/rbk_test/trader.ex

Стартует приложение в lib/rbk_test/application.ex как otp app

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `rbk_test` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:rbk_test, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/rbk_test](https://hexdocs.pm/rbk_test).

