defmodule RbkTest.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @types ~w(altruist dishonest sly less_vindictive weird vindictive)a
  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      {RbkTest.Guild, @types},
      {RbkTest.Vault, @types}
      # Supervisor.Spec.supervisor(RbkTest.Guild, [nil])
      # Starts a worker by calling: RbkTest.Worker.start_link(arg)
      # {RbkTest.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RbkTest.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
