defmodule RbkTest.Guild do
  use Supervisor

  def start_link(types) do
     Supervisor.start_link(__MODULE__, types, name: :guild)
  end

  @per_type 10
  def init(types) do
    children = Enum.flat_map(types, fn(type)->
      Enum.map (1..@per_type), fn(i)->
        %{
          id: String.to_atom("#{type}_#{i}"),
          start: {RbkTest.Trader, :start_link, [type]}
         }
      end
    end)

    Supervisor.init(children, strategy: :one_for_all)
  end

  def run_for_a_year do
    :guild
    |> Supervisor.which_children()
    |> get_and_prepare_traders()
    |> update_guild_members()
    |> pair_up_and_count_deals()
    # |> Enum.map(fn({[pid1, pid2], num_of_deals}) ->
    #   Task.async fn->
    #     Enum.each (1..num_of_deals), fn(_) ->
    #       RbkTest.Trader.make_a_deal(pid1, pid2)
    #     end
    #   end
    # end)
    # |> Enum.map(&Task.await/1)
    |> Enum.each(fn({[pid1, pid2], num_of_deals}) ->
      Enum.each (1..num_of_deals), fn(_) ->
        RbkTest.Trader.make_a_deal(pid1, pid2)
      end
    end)
  end

  defp get_and_prepare_traders(children) do
    Enum.map children, fn({_, pid, _, _})-> pid end
  end

  defp update_guild_members([ first_pid | _ ] = trader_pids) do
    case RbkTest.Trader.get_state(first_pid) do
      %{ history: [] } -> trader_pids
      _ ->
        year_stats = get_year_stats(trader_pids)
        remove_the_worst year_stats
        print_year_stats year_stats

        states = Enum.map trader_pids, &RbkTest.Trader.get_state/1
        print_stats_per_type states
        print_total_gold()

        trader_pids
    end
  end

  def print_total_gold, do: IO.inspect RbkTest.Vault.get_stats()

  defp get_year_stats(trader_pids) do
    trader_pids
    |> Enum.reduce([], fn(pid, acc) ->
      %{ gold: gold, type: type } = RbkTest.Trader.get_state(pid)
      RbkTest.Vault.log_gold type, gold
      RbkTest.Trader.prepare_for_next_year(pid)
      [ {pid, gold, type} | acc ]
    end)
    |> Enum.sort(fn({_, g1, _}, {_, g2, _})-> g1 >= g2 end)
  end

  defp remove_the_worst(stats) do
    the_best = Enum.take(stats, 12)
    stats
    |> Enum.reverse() # reverse to take the worst traders
    |> Enum.take(12)
    |> Enum.zip(the_best)
    |> Enum.each(fn({{pid, _, _},{_, _, type}})->
      RbkTest.Trader.change_trader pid, type
    end)
  end

  defp print_year_stats(stats) do
    IO.puts "------ YEAR STATS: -------"
    Enum.each stats, fn({_pid, gold, type})->
      IO.puts "trader of type #{type} earned #{gold} this year"
    end
    IO.puts "--------------------------"
  end

  defp print_stats_per_type(states) do
    IO.puts "------ STATS PER TYPE: -------"
    states
    |> Enum.reduce(%{}, fn(%{ type: type, total_gold: total_gold }, acc) ->
      new_val = Map.get acc, type, []
      Map.put acc, type, [ total_gold | new_val ]
    end)
    |> IO.inspect()
    |> Enum.each(fn({type, list_of_total_gold})->
      IO.puts "Total #{Enum.count(list_of_total_gold)} of traders of type #{type} earned totally #{Enum.sum(list_of_total_gold)}"
    end)
    IO.puts "------------------------------"
  end

  defp pair_up_and_count_deals(trader_pids) do
    trader_pids
    |> Combination.combine(2)
    |> Enum.map(&({&1, Enum.random(5..10)}))
  end
end
