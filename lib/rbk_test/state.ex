defmodule RbkTest.State do
  # TODO: keep history as map with key as pid, and value as list of operations
  defstruct type: nil, gold: 0, total_gold: 0, history: [], util: nil, global_history: []
end
