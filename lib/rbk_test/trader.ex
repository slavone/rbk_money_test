defmodule RbkTest.Trader do
  use GenServer

  def start_link(type) do
    GenServer.start_link(__MODULE__, %RbkTest.State{type: type})
  end

  def make_a_deal(my_pid, trader_pid) when my_pid != trader_pid do
    GenServer.call my_pid, {:make_a_deal, trader_pid}
  end

  @moves [:honest, :scam]
  def init_trade(trader_pid, move) when move in @moves do
    GenServer.call trader_pid, {:init_trade, move}
  end

  def get_state(pid) do
    GenServer.call pid, :get_state
  end

  def change_trader(pid, new_type) do
    GenServer.call pid, {:change_trader, new_type}
  end

  def clear_history(pid) do
    GenServer.call pid, :clear_history
  end

  def prepare_for_next_year(pid) do
    GenServer.call pid, :prepare_for_next_year
  end

  def handle_call({:make_a_deal, trader_pid}, _from, %{gold: gold, history: history} = state) do
    my_move = simulate_error(calc_move(trader_pid, state))
    other_move = __MODULE__.init_trade trader_pid, my_move
    profit = calc_profit(my_move, other_move)
    new_operation = %RbkTest.Operation{ from: trader_pid, my_move: my_move, other_move: other_move}

    {:reply, "Other move was #{other_move}, made profit #{profit}", %RbkTest.State{ state | gold: gold + profit, history: [ new_operation | history ] }}
  end

  def handle_call({:init_trade, other_move}, {from, _some_id}, %{gold: gold, history: history} = state) do
    my_move = simulate_error(calc_move(from, state))
    # my_move = calc_move(from, state)
    profit = calc_profit(my_move, other_move)
    new_operation = %RbkTest.Operation{ from: from, my_move: my_move, other_move: other_move}

    {:reply, my_move, %RbkTest.State{ state | gold: gold + profit, history: [ new_operation | history ] }}
  end

  def handle_call({:change_trader, new_type}, _from, %{ type: old_type }) do
    IO.puts "Trader of type #{old_type} changed to type #{new_type}"
    new_state = %RbkTest.State{type: new_type}
    {:reply, new_state, new_state}
  end

  def handle_call(:clear_history, _from, %{ history: old_history, global_history: global_history } = state) do
    new_state = %{ state | history: [], global_history: [ old_history | global_history ] }
    {:reply, new_state, new_state}
  end

  def handle_call(:prepare_for_next_year, _, %{ history: old_history, global_history: global_history, gold: gold, total_gold: total_gold } = state) do
    new_state = %{ state | history: [], global_history: [ old_history | global_history ], gold: 0, total_gold: total_gold + gold }
    {:reply, new_state, new_state}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  defp calc_profit(my_move, other_move)
  defp calc_profit(:honest, :honest), do: 4
  defp calc_profit(:honest, :scam), do: 1
  defp calc_profit(:scam, :honest), do: 5
  defp calc_profit(:scam, :scam), do: 2

  defp simulate_error(move) do
    case :rand.uniform(100) <= 5 do
      true -> switch_move(move)
      false -> move
    end
  end

  defp switch_move(:honest), do: :scam
  defp switch_move(:scam), do: :honest

  defp calc_move(_, %{type: :altruist}) do
    :honest
  end

  defp calc_move(_, %{type: :random}) do
    case :rand.uniform(100) <= 10 do
      true -> :honest
      false -> :scam
    end
  end

  defp calc_move(_, %{type: :dishonest}) do
    :scam
  end

  defp calc_move(_, %{type: :wildcard}) do
    Enum.random @moves
  end

  defp calc_move(from, %{type: :sly, history: history}) do
    case Enum.find history, &(&1.from == from) do
      nil -> :honest
      %RbkTest.Operation{other_move: other_move} -> other_move
    end
  end

  defp calc_move(from, %{type: :weird, history: history}) do
    case Enum.count history, &(&1.from == from) do
      0 -> :honest
      1 -> :scam
      2 -> :honest
      3 -> :honest
      _ ->
        case Enum.find history, &(&1.from == from && &1.other_move == :scam) do
          nil -> calc_move(from, %{type: :sly, history: history})
          %RbkTest.Operation{} -> calc_move(from, %{type: :dishonest})
        end
    end
  end

  defp calc_move(from, %{type: :vindictive, history: history}) do
    case Enum.find history, &(&1.from == from && &1.other_move == :scam) do
      nil -> :honest
      %RbkTest.Operation{} -> :scam
    end
  end

  defp calc_move(from, %{type: :less_vindictive, history: history}) do
    case Enum.count history, &(&1.from == from && &1.other_move == :scam) do
      0 -> :honest
      1 -> :honest
      _  -> :scam
    end
  end
end
