defmodule RbkTest.Vault do
  use GenServer

  defstruct total_gold: %{}

  def start_link(types) do
    total_gold = Enum.reduce(types, %{}, &(Map.put(&2, &1, 0)))
    GenServer.start_link(__MODULE__, %__MODULE__{ total_gold: total_gold }, name: __MODULE__)
  end

  def log_gold(type, gold) do
    GenServer.call __MODULE__, {:log_gold, type, gold}
  end

  def get_stats do
    GenServer.call __MODULE__, :get_stats
  end

  def handle_call({:log_gold, type, gold}, _from,  %{ total_gold: total_gold } = state) do
    %{ ^type => current_gold } = total_gold
    new_gold_val = current_gold + gold
    {:reply, new_gold_val, %{ state | total_gold: %{ total_gold | type => new_gold_val } }}
  end

  def handle_call(:get_stats, _from, %{ total_gold: total_gold } = state) do
    {:reply, total_gold, state}
  end
end
